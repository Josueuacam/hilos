/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hilos;


import static java.lang.Math.random;
import java.lang.Math;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author CHARL
 */
public class Carros extends Thread{
    private JLabel etiqueta;
    private Carrera auto;

    public JLabel getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(JLabel etiqueta) {
        this.etiqueta = etiqueta;
    }

    public Carrera getAuto() {
        return auto;
    }

    public void setAuto(Carrera auto) {
        this.auto = auto;
    }
    
    public Carros(JLabel etiqueta, Carrera auto) {
        this.etiqueta = etiqueta;
        this.auto = auto;
    }
    
    
    @Override
    public void run(){
        int auto1 = 0;
        int auto2 = 0;
        
        while(true){
            
            try {
                sleep((int)(Math.random()*1000));
                auto1 = auto.getPrimerAuto().getLocation().x;
                auto2 = auto.getSegundoAuto().getLocation().x;
                
               // etiqueta.setLocation(etiqueta.getLocation().x + 10, etiqueta.getLocation().y);
              //  auto.repaint();
                
                if(auto1 < auto.getBarrera().getLocation().x- 125 && auto2 < auto.getBarrera().getLocation().x - 125){
                    etiqueta.setLocation(etiqueta.getLocation().x + 10, etiqueta.getLocation().y);
                    auto.repaint();
                
                }else{
                    break;
                }
                
                
            } catch (Exception e) {
                System.out.println(e);
            }
 
        
          if(etiqueta.getLocation().x >= auto.getBarrera().getLocation().x - 125){
              if(auto1 > auto2){
                  JOptionPane.showMessageDialog(null, "Gano el carro azul");
              }
              else if (auto2 > auto1){
                  JOptionPane.showMessageDialog(null, "Gano el carro rojo");
              }
              else{
                  JOptionPane.showMessageDialog(null, " oh no, hubo un empate");
              }
          }
    }
    }
}
