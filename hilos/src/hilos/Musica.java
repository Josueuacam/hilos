/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hilos;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jlgui.basicplayer.*;

/**
 *
 * @author CHARL
 */
public class Musica {

    BasicPlayer player = new BasicPlayer();

    public Musica(String ruta) {
        try {
            player.open(new File(ruta));
        } catch (BasicPlayerException ex) {
            Logger.getLogger(Musica.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void play() {
        try {
            player.play();
            System.out.println(player.getMaximumGain());
        } catch (BasicPlayerException ex) {
            Logger.getLogger(Musica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pausa() {
        try {
            player.pause();
        } catch (BasicPlayerException ex) {
            Logger.getLogger(Musica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void continuar() {
        try {
            player.resume();
        } catch (BasicPlayerException ex) {
            Logger.getLogger(Musica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void stop() {
        try {
            player.stop();
        } catch (BasicPlayerException ex) {
            Logger.getLogger(Musica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
